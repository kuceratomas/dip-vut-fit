package demo.gameelements;

import eis.iilang.*;
import genalg.interfaces.GAAgent;
import genalg.interfaces.GAJob;

import java.io.Serializable;
import java.util.*;

import demo.communication.Agent;
import demo.communication.MailService;

/**
 * A TravelAgent for the 2018 City scenario.
 * 
 * @author t-ah, https://github.com/agentcontest/massim_2018/commits?author=t-ah
 * @author Tomas Kucera, xkucer90
 * 
 */
public class TravelAgent extends Agent implements GAAgent, Serializable{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private State state = State.EXPLORE;
	
	private Game game;

	private String agentName;
	private String role = "";
	private int skill = 0;
	private int speed = 0;
	private int load = 0;
    private int loadMax = 0;
    
    private double lat = 0;
    private double lon = 0;

    private Map<String, Integer> carriedItems;

	private int charge = 0;
	private int battery = 0;

    private String lastAction = "";
    private String lastActionResult = "";

    // to avoid duplicates in game.jobList
    private Map<String, GAJob> jobMap;
    
    private String leader = "";
    private transient Loc exploreTarget;
    private transient Loc chargingTarget;
    
    private Random rand = new Random(17);

    /**
     * Constructor
     *
     * @param name    the agent's name
     * @param mailbox the mail facility
     */
    public TravelAgent(String name, MailService mailbox) {
        super(name, mailbox);
        this.agentName = name;
        
        carriedItems = new LinkedHashMap<String, Integer>();
        jobMap = new HashMap<String, GAJob>();
        
    }

    @Override
    public void handlePercept(Percept percept) {}

    @Override
    public Action step() {
    	
    	// "elect" a leader
        if(leader.equals("")) {
            broadcast(new Percept("leader"), getName());
            this.leader = getName();
            
            this.game = new Game();
            broadcastGame(game, getName());
            
        }

        // parse percepts
        List<Percept> percepts = getPercepts();
        for (Percept percept : percepts)
        {
        	
        	
        	if (getName().equals(leader))
        	{
        		switch(percept.getName())
        		{
	                case "job":
	                    //jobs.put(getStringParam(percept, 0), percept);
	                    ParameterList itemsPar = (ParameterList) percept.getParameters().get(5);
	                    String jobName = getStringParam(percept, 0);
	                    int reward = getIntParam(percept, 2);
	                    Map<String, Integer> itemsMap = new HashMap<String, Integer>();
	                    itemsPar.forEach(item -> {
	                    	String itemName = ((Identifier)((Function) item).getParameters().get(0)).getValue();
	                    	int amount = ((Numeral)((Function) item).getParameters().get(1)).getValue().intValue();
	                    	itemsMap.put(itemName, amount);
	                    });
	                    jobMap.put(jobName, new Job(jobName, itemsMap, reward));
	                    //jobList.add(new Job(jobName, itemsMap, reward, start, end));
	                    //System.out.println("new job: " + jobName);
	                    break;
	               
	                case "item":
	                	String itemName = getStringParam(percept, 0);
	                	int volume = getIntParam(percept, 1);
	                	Set<String> parts = new HashSet<String>();
	                	Set<String> roles = new HashSet<String>();
	
	                	ParameterList partsFunction = (ParameterList)((Function)percept.getParameters().get(3)).getParameters().get(0);
	                	ParameterList rolesFunction = (ParameterList)((Function)percept.getParameters().get(2)).getParameters().get(0);
	                	
	                	partsFunction.forEach(part ->parts.add(((Identifier) part).getValue()));
	                	rolesFunction.forEach(role ->roles.add(((Identifier) role).getValue()));
	                	game.addItem(new Item(itemName, parts, roles, volume));
	                	break;
	                case "minLat": game.setMinLat(getDoubleParam(percept, 0)); break;
	                case "maxLat": game.setMaxLat(getDoubleParam(percept, 0)); break;
	                case "minLon": game.setMinLon(getDoubleParam(percept, 0)); break;
	                case "maxLon": game.setMaxLon(getDoubleParam(percept, 0)); break;
	                
	                case "chargingStation": game.addChargingStation(getStringParam(percept, 0), percept); break;
        		}
        	}
        	
        	switch(percept.getName())
    		{
	    		case "role":
	                role = getStringParam(percept, 0);
	                battery = getIntParam(percept, 9);
	                loadMax = getIntParam(percept, 4);
	                break;
	            case "load":
	            	load = getIntParam(percept, 0);
	            	break;
	            case "skill":
	             	skill = getIntParam(percept, 0);
	             	break;
	            case "speed":
	            	speed = getIntParam(percept, 0);
	             	break;
	            case "charge":
	                charge = getIntParam(percept, 0);
	                break;
	    		case "resourceNode":
	                 sendMessage(percept, leader, getName());
	                 say("Found a resource node.");
	                 break;
	    		case "lat": lat = getDoubleParam(percept, 0);break;
	            case "lon": lon = getDoubleParam(percept, 0);break;
	            case "lastAction": lastAction = getStringParam(percept, 0); break;
	            case "lastActionResult": lastActionResult = getStringParam(percept, 0); break;
    		}
        }
        	
        say("My last action was " + lastAction + " : " + lastActionResult);
        
       
        if (getName().equals(leader))
        {
        	 // prepare data structures for gen alg computation
        	game.setPlannedJobs(new ArrayList<GAJob>(jobMap.values()));
            countLeafItems();
            
            // calculate hypotenuse of the playing field
            game.setHypotenuse(Math.sqrt(Math.pow(game.getMaxLat() - game.getMinLat(), 2) + Math.pow(game.getMaxLon() - game.getMinLon(), 2)));
        }

        return act();
    }

    
    private void countLeafItems()
    {
    	// fill job items with Item object references
    	for (GAJob job : game.getPlannedJobs())
    	{
    		Set<String> itemNames = ((Job)job).getItems().keySet();
    		for(String item : itemNames)
    		{		
    			goToParts(job, item, ((Job)job).getItems().get(item));
    		}
    	}
    }
    
    private void goToParts(GAJob job, String item, Integer count)
    {
    	if (((Item)game.getItems().get(item)).getParts().isEmpty())
    	{
    		((Job)job).mergeLeafItems(item, count);
    	}
    	else
    	{
    		for (String role : ((Item)game.getItems().get(item)).getRoles())
    		{
    			job.getDutiesByRole().merge(role, count, Integer::sum);
    			((Job)job).setTotalDuties(((Job)job).getTotalDuties() + count);
    		}

    		for (String part : ((Item)game.getItems().get(item)).getParts())
    		{
    			goToParts(job, part, count);
    		}
    	}
    }

    /**
     * choose an action
     * @return an action
     */
    private Action act() {

        if(charge < .3 * battery) {
            state = State.RECHARGE;
            String station = "";
            double minDist = Double.MAX_VALUE;
            for(Percept p: game.getChargingStations().values()) {
                double cLat = getDoubleParam(p, 1);
                double cLon = getDoubleParam(p, 2);
                double dist = Math.sqrt(Math.pow(lat - cLat, 2) + Math.pow(lon - cLon, 2));
                if(dist < minDist) {
                    minDist = dist;
                    station = getStringParam(p, 0);
                }
            }
            if(!station.equals("")) {
                Percept p = game.getChargingStations().get(station);
                chargingTarget = new Loc(getDoubleParam(p, 1), getDoubleParam(p, 2));
            }
        }

        if(state == State.RECHARGE) {
            if(charge >= battery) {
            	state = State.EXPLORE;
            }
            else {
                if (chargingTarget != null) {
                    if (atLoc(chargingTarget)) {
                        return new Action("charge");
                    }
                    else {
                        return new Action("goto", new Numeral(chargingTarget.lat), new Numeral(chargingTarget.lon));
                    }
                }
            }
        }

        if(exploreTarget != null) {
            if(atLoc(exploreTarget)) {
                // target reached
                exploreTarget = null;
            }
        }

        if(state == State.EXPLORE) {
            if(exploreTarget == null || lastActionResult.equalsIgnoreCase("failed_no_route")) {
                double expLat = game.getMinLat() + rand.nextDouble() * (game.getMaxLat() - game.getMinLat());
                double expLon = game.getMinLon() + rand.nextDouble() * (game.getMaxLon() - game.getMinLon());
                exploreTarget = new Loc(expLat, expLon);
            }
            return new Action("goto", new Numeral(exploreTarget.lat), new Numeral(exploreTarget.lon));
        }

        return new Action("continue");
    }

    private boolean atLoc(Loc loc) {
        return Math.abs(lat - loc.lat) < .0001 && Math.abs(lon - loc.lon) < .0001;
    }
    

    private String getStringParam(Percept percept, int position) {
        Parameter p = percept.getParameters().get(position);
        if (p instanceof Identifier) return ((Identifier) p).getValue();
        return "";
    }

    private int getIntParam(Percept percept, int position) {
        Parameter p = percept.getParameters().get(position);
        if (p instanceof Numeral) return ((Numeral) p).getValue().intValue();
        return 0;
    }

    private double getDoubleParam(Percept percept, int position) {
        Parameter p = percept.getParameters().get(position);
        if (p instanceof Numeral) return ((Numeral) p).getValue().doubleValue();
        return 0;
    }

    @Override
    public void handleMessage(Percept message, String sender) {
        switch(message.getName()) {
            case "leader":
                this.leader = sender;
                break;
            case "resourceNode":
                double resLat = getDoubleParam(message, 1);
            	double resLon = getDoubleParam(message, 2);
            	String nodeName = getStringParam(message, 0);
            	String resName = getStringParam(message, 3);
                ResourceNode resNode = new ResourceNode(nodeName, resName, resLat, resLon);
                
                game.addResNode(resNode);
                
                break;
            default:
                say("ERR: " + message.getName());
        }
    }
    
    @Override
    public void handleGame(Game game, String sender) {
    	this.game = game;
    }
    
    
    
    public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	/**
	 * @see genalg.interfaces.GAAgent#getGame()
	 */
	public Game getGame() {
		return game;
	}

	public void setGame(Game game) {
		this.game = game;
	}

	/**
	 * @see genalg.interfaces.GAAgent#getRole()
	 */
	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	/**
	 * @see genalg.interfaces.GAAgent#getSkill()
	 */
	public int getSkill() {
		return skill;
	}

	public void setSkill(int skill) {
		this.skill = skill;
	}
	
	/**
	 * @see genalg.interfaces.GAAgent#getSpeed()
	 */
	public int getSpeed() {
		return speed;
	}

	/**
	 * @see genalg.interfaces.GAAgent#getLoad()
	 */
	public int getLoad() {
		return load;
	}

	public void setLoad(int load) {
		this.load = load;
	}

	/**
	 * @see genalg.interfaces.GAAgent#getLoadMax()
	 */
	public int getLoadMax() {
		return loadMax;
	}

	public void setLoadMax(int loadMax) {
		this.loadMax = loadMax;
	}

	/**
	 * @see genalg.interfaces.GAAgent#getLat()
	 */
	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	/**
	 * @see genalg.interfaces.GAAgent#getLon()
	 */
	public double getLon() {
		return lon;
	}

	public void setLon(double lon) {
		this.lon = lon;
	}

	/**
	 * @see genalg.interfaces.GAAgent#getCarriedItems()
	 */
	public Map<String, Integer> getCarriedItems() {
		return carriedItems;
	}

	public void setCarriedItems(Map<String, Integer> carriedItems) {
		this.carriedItems = carriedItems;
	}
	
	public void addCarriedItem(String itemName, int quantity) {
		this.carriedItems.put(itemName, quantity);
	}
    
    

    class Loc {
        double lat;
        double lon;
        Loc(double lat, double lon) {
            this.lat = lat;
            this.lon = lon;
        }
    }

    enum State {
        EXPLORE, RECHARGE
    }


}