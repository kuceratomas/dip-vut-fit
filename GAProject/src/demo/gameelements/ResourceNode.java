package demo.gameelements;

import java.io.Serializable;

import genalg.interfaces.GAResourceNode;

public class ResourceNode implements GAResourceNode, Serializable {
	
	private static final long serialVersionUID = 1L;
	private int number;
	private String resource;
	private String name;
	private Double lat;
	private Double lon;

	public ResourceNode(String name, String resource, double lat, double lon)
	{
		number = Integer.valueOf(name.replaceAll("\\D+",""));
		this.name = name;
		this.resource = resource;
		this.lat = lat;
		this.lon = lon;
	}
	
	public String getResource() {
		return resource;
	}

	public void setResource(String resource) {
		this.resource = resource;
	}

	public Double getLat() {
		return lat;
	}

	public void setLat(Double lat) {
		this.lat = lat;
	}

	public Double getLon() {
		return lon;
	}

	public void setLon(Double lon) {
		this.lon = lon;
	}

	public boolean equals(Object obj)
	{
		return this.name.equals(((ResourceNode)obj).name);
	}
	
	public int hashCode()
	{
		return number;
	}
	
}
