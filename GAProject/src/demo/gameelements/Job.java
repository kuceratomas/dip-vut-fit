package demo.gameelements;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import genalg.interfaces.GAJob;

public class Job implements GAJob, Serializable {

	private static final long serialVersionUID = 1L;
	private String name;
	private int reward;
	private Map<String, Integer> items;
	private int number;
	private Map<String, Integer> leafItems =  new LinkedHashMap<String, Integer>();
	private Map<String, Integer> dutiesByRole = new LinkedHashMap<String, Integer>();
	private int totalDuties = 0;

	
	public int getTotalDuties() {
		return totalDuties;
	}

	public void setTotalDuties(int totalDuties) {
		this.totalDuties = totalDuties;
	}

	/**
	 * @see genalg.interfaces.GAJob#getReward()
	 */
	public int getReward() {
		return reward;
	}

	public void setReward(int reward) {
		this.reward = reward;
	}

	public Map<String, Integer> getItems() {
		return items;
	}


	public void setItems(Map<String, Integer> items) {
		this.items = items;
	}

	/**
	 * @see genalg.interfaces.GAJob#getLeafItems()
	 */
	public Map<String, Integer> getLeafItems() {
		return leafItems;
	}

	public void setLeafItems(Map<String, Integer> leafItems) {
		this.leafItems = leafItems;
	}
	
	public void mergeLeafItems(String item, int count) {
		this.leafItems.merge(item, count, Integer::sum);
	}

	/**
	 * @see genalg.interfaces.GAJob#getDutiesByRole()
	 */
	public Map<String, Integer> getDutiesByRole() {
		return dutiesByRole;
	}

	public void setDutiesByRole(Map<String, Integer> dutiesByRole) {
		this.dutiesByRole = dutiesByRole;
	}
	
	public Job(String name, Map<String, Integer> items, int reward) {
		number = Integer.valueOf(name.replaceAll("\\D+",""));
		this.name = name;
        this.items = items;
        this.reward = reward;

    }
	
	
	public boolean equals(Object obj)
	{
		return this.name.equals(((Job)obj).name);
	}
	
	public int hashCode()
	{
		return number;
	}

	public String getName() {
		return name;
	}

	
	

}
