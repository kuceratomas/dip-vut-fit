package demo.gameelements;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import eis.iilang.Percept;
import genalg.interfaces.GAGame;
import genalg.interfaces.GAItem;
import genalg.interfaces.GAJob;
import genalg.interfaces.GAResourceNode;

/**
 * Game stores common info about the environment.
 * 
 * @author Tomas Kucera, xkucer90
 *
 */
public class Game  implements GAGame, Serializable{

	private static final long serialVersionUID = 1L;
	
	private double minLat = 0;
	private double maxLat = 0;
	private double minLon = 0;
	private double maxLon = 0;
	private double hypotenuse = 0;
    
	private List<GAJob> plannedJobs = new ArrayList<GAJob>();
    
	private Map<String, GAItem> items = new LinkedHashMap<String, GAItem>();
	
	private Set<String> allRoles = new HashSet<String>();
    
	private Map<String, Set<GAResourceNode>> resNodesByResName = new LinkedHashMap<String, Set<GAResourceNode>>();
	private Map<String, Percept> chargingStations = new HashMap<>();
	
	
	
	
	/**
	 * @see genalg.interfaces.GAGame#getAllRoles()
	 */
	public Set<String> getAllRoles() {
		return allRoles;
	}
	public void setAllRoles(Set<String> allRoles) {
		this.allRoles = allRoles;
	}
	
	public void addRole(String role) {
		this.allRoles.add(role);
	}
	
	public double getMinLat() {
		return minLat;
	}
	public void setMinLat(double minLat) {
		this.minLat = minLat;
	}
	public double getMaxLat() {
		return maxLat;
	}
	public void setMaxLat(double maxLat) {
		this.maxLat = maxLat;
	}
	public double getMinLon() {
		return minLon;
	}
	public void setMinLon(double minLon) {
		this.minLon = minLon;
	}
	public double getMaxLon() {
		return maxLon;
	}
	public void setMaxLon(double maxLon) {
		this.maxLon = maxLon;
	}
	public double getHypotenuse() {
		return hypotenuse;
	}
	public void setHypotenuse(double hypotenuse) {
		this.hypotenuse = hypotenuse;
	}
	/**
	 * @see genalg.interfaces.GAGame#getPlannedJobs()
	 */
	public List<GAJob> getPlannedJobs() {
		return plannedJobs;
	}
	public void setPlannedJobs(List<GAJob> jobList) {
		this.plannedJobs = jobList;
	}
	/**
	 * @see genalg.interfaces.GAGame#getItems()
	 */
	public Map<String, GAItem> getItems() {
		return items;
	}
	public void setItems(Map<String, GAItem> items) {
		this.items = items;
	}
	public void addItem(GAItem item) {
		this.items.put(((Item)item).getName(), item);
	}
	/**
	 * @see genalg.interfaces.GAGame#getResNodes()
	 */
	public Map<String, Set<GAResourceNode>> getResNodes() {
		return resNodesByResName;
	}
	public void setResNodes(Map<String, Set<GAResourceNode>> resNodesByResName) {
		this.resNodesByResName = resNodesByResName;
	}
	public void addResNode(ResourceNode resNode) {
		this.resNodesByResName.computeIfAbsent(resNode.getResource(), k -> new HashSet<>()).add(resNode);
	}
	public Map<String, Percept> getChargingStations() {
		return chargingStations;
	}
	public void setChargingStations(Map<String, Percept> chargingStations) {
		this.chargingStations = chargingStations;
	}
	public void addChargingStation(String name, Percept percept) {
		this.chargingStations.put(name, percept);
	}

	
    
}
