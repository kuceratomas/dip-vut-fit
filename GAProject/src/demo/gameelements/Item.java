package demo.gameelements;

import java.io.Serializable;
import java.util.Set;

import genalg.interfaces.GAItem;

/**
 * Item class.
 * 
 * @author Tomas Kucera, xkucer90
 *
 */
public class Item implements GAItem, Serializable{
	
	private static final long serialVersionUID = 1L;
	private String name;
	private int volume;
	private Set<String> parts;
	private Set<String> roles;
	
    public Item(String name, Set<String> parts, Set<String> roles, int volume) {
    	this.name = name;
        this.volume = volume;
        this.parts = parts;
        this.roles = roles;
    }
    
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @see genalg.interfaces.GAItem#getVolume()
	 */
	public int getVolume() {
		return volume;
	}

	public void setVolume(int volume) {
		this.volume = volume;
	}

	public Set<String> getParts() {
		return parts;
	}

	public void setParts(Set<String> parts) {
		this.parts = parts;
	}

	public Set<String> getRoles() {
		return roles;
	}

	public void setRoles(Set<String> roles) {
		this.roles = roles;
	}

	
    
}
