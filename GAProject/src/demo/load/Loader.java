package demo.load;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import demo.gameelements.Job;
import demo.gameelements.TravelAgent;
import genalg.Evolution;
import genalg.interfaces.GAAgent;

/**
 * @author Tomas Kucera, xkucer90
 *
 */
public class Loader {

	/**
	 * Entry point of the agent loading.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		try
		{
			// load agents from the file
    		FileInputStream fis = new FileInputStream("agents1.tmp");
    		ObjectInputStream ois = new ObjectInputStream(fis);
    		@SuppressWarnings("unchecked")
    		Map<String, TravelAgent> loadedAgents = (Map<String, TravelAgent>) ois.readObject();
    		ois.close();
    		List<TravelAgent> agentList = new ArrayList<TravelAgent>(loadedAgents.values());
    		
    		for (int i = 0; i < agentList.size(); i++)
    		{
    			// add role to the list of all the roles
    			agentList.get(0).getGame().addRole(agentList.get(i).getRole());
    			
    			// add some carried items for the agents
    			Editor.editCarriedItems(i, agentList.get(i));
    		}
    		
    		
    		
    		// editting of the agent/job specifications (add deeper meaning to the GA)
    		Editor.editJobItems(agentList.get(0).getGame().getPlannedJobs());
			Editor.editJobRoles(agentList.get(0).getGame().getPlannedJobs(), agentList.get(0).getGame().getAllRoles());
    		
    		// start the GA
			List<GAAgent> gaagents = new ArrayList<GAAgent>(loadedAgents.values());
			
			List<Integer> solution = Evolution.calculate(gaagents);

			System.out.println("Final solution:\n" + solution + "\n\nCrucial properties: \n");
			
			
			Map<Integer, ArrayList<Integer>> jobAssignments = new HashMap<Integer, ArrayList<Integer>>();
			for (int i = 0; i < solution.size(); i++) // entire solution
			{
				jobAssignments.computeIfAbsent(solution.get(i), k -> new ArrayList<>()).add(i);
			}
			
			for (Map.Entry<Integer, ArrayList<Integer>> jobEntry : jobAssignments.entrySet())
			{
				int jobNumber = jobEntry.getKey();
				Job actualJob = (Job)gaagents.get(0).getGame().getPlannedJobs().get(jobNumber);
			    List<Integer> assignedAgentNumbers = (ArrayList<Integer>) jobEntry.getValue();
			    
			   
			    String jobString = "name: " + actualJob.getName() + "\n";
			    jobString += "rew: " + actualJob.getReward() + "\n";
			    jobString += "required roles: " + actualJob.getDutiesByRole() + "\n";
			    jobString += "items: " + actualJob.getItems() + "\n";
			    jobString += "basic items: " + actualJob.getLeafItems() + "\n";
			    System.out.println(jobString);
			    
			    for (int agentNumber : assignedAgentNumbers)
			    {
			    	String agentString = "role: " + gaagents.get(agentNumber).getRole() + ", ";
			    	agentString += "skill: " + gaagents.get(agentNumber).getSkill() + ", ";
					agentString += "load: " + gaagents.get(agentNumber).getLoad() + ", ";
					agentString += "maxLoad: " + gaagents.get(agentNumber).getLoadMax() + ", ";
					agentString += "speed: " + gaagents.get(agentNumber).getSpeed() + ", ";
					agentString += "lat: " + gaagents.get(agentNumber).getLat() + ", ";
					agentString += "lon: " + gaagents.get(agentNumber).getLon() + "\n";
					agentString += "carries: " + gaagents.get(agentNumber).getCarriedItems() + ";";
					
			    	System.out.println(agentString);
			    }
			    System.out.println("---------------------------");
			}
    		
    		
		} catch (IOException | ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
