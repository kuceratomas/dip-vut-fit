package demo.load;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import demo.gameelements.Job;
import demo.gameelements.TravelAgent;
import genalg.interfaces.GAJob;

/**
 * @author Tomas Kucera, xkucer90
 *
 */
public class Editor {

	/**
	 * @param jobs planned job list
	 */
	public static void editJobItems(List<GAJob> jobs)
	{
		// get the list of all the basic items
		List<String> basicItems = new ArrayList<String>(((Job)jobs.get(0)).getLeafItems().keySet());
		// for every job delete one of the basic items
		for (int i = 0; i < jobs.size(); i++)
		{
			((Job)jobs.get(i)).getLeafItems().remove(basicItems.get(i%basicItems.size()));
		}
	}
	
	/**
	 * @param jobs planned job list
	 * @param allRoles all the existing role in the scenario
	 */
	public static void editJobRoles(List<GAJob> jobs, Set<String> allRoles)
	{
		// get the list of all the roles
		List<String> allRolesList = new ArrayList<String>(allRoles);
		// for every job delete one of the basic items
		for (int i = 0; i < jobs.size(); i++)
		{
			String roleToRemove = allRolesList.get(i%allRolesList.size());
			((Job)jobs.get(i)).setTotalDuties(((Job)jobs.get(i)).getTotalDuties() - jobs.get(i).getDutiesByRole().get(roleToRemove));
			jobs.get(i).getDutiesByRole().remove(roleToRemove);
		}
	}
	
	/**
	 * @param agentIndex index of an actual processed agent
	 * @param agent processed agent
	 */
	public static void editCarriedItems(int agentIndex, TravelAgent agent)
	{
		// need to get items from knownResNodes to prevent agent from having unreachable item
		List<String> leafItems = new ArrayList<String>(agent.getGame().getResNodes().keySet());

		String item = leafItems.get(agentIndex % leafItems.size());
		int itemVolume = agent.getGame().getItems().get(item).getVolume();
		int maxItems = (agent.getLoadMax()) / itemVolume; // max number of items based on the agent's max load
		
		if (maxItems > 1)
		{
			int quantity = (73 * agentIndex) % (maxItems - 1) + 1; // generate "pseudorandom" quantity
			agent.addCarriedItem(item, quantity);
			agent.setLoad(quantity * itemVolume + agent.getLoad());
		}
		
		
		
	}
	
}
