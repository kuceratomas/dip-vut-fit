package demo.communication;

import java.io.File;

import eis.exceptions.ManagementException;
import eis.iilang.EnvironmentState;
import massim.eismassim.EnvironmentInterface;


/**
 * Starter of the environment and top level performer of agent's steps.
 * 
 * @author t-ah, https://github.com/agentcontest/massim_2018/commits?author=t-ah
 * @author Tomas Kucera, xkucer90
 *
 */
public class EnvironmentStarter {

	/**
	 * Entry point of the game.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		
		// directory of the .json files for server configuration
		String configDir = "conf\\TravelAgents";
		
		Scheduler scheduler = new Scheduler(configDir);
		
		System.out.println(configDir + File.separator + "eismassimconfig.json");
		
		// start the communication interface
		EnvironmentInterface ei = new EnvironmentInterface(configDir + File.separator + "eismassimconfig.json");
		try {
			ei.start();
		} catch (ManagementException e) {
			e.printStackTrace();
		}

		scheduler.setEnvironment(ei);
		
		int step = 0;
        while ((ei.getState() == EnvironmentState.RUNNING)) {
            System.out.println("SCHEDULER STEP " + step);
            scheduler.step();
            step++;
        }
	}
}
